#+TITLE: Bash track on Exercism
#+AUTHOR: Frédéric Santos

My solutions for the Bash track on [[https://exercism.io/][Exercism]].
