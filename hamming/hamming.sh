#!/usr/bin/env bash

# Handle cases of no input:
if (( $# < 2 ))
then
    echo "Usage: hamming.sh <string1> <string2>"
    exit 1
fi

# Check lengths of the two args:
if (( ${#1} != ${#2} ))
then
    echo "left and right strands must be of equal length"
    exit 1
fi

# Parse and compare the two strands:
diffs=0                         # counter of differences
for (( i=0; i<${#1}; i++ ))
do
    if [[ "${1:i:1}" != "${2:i:1}" ]]
    then
        (( diffs++ ))
    fi
done

# Display the number of differences:
echo $diffs
