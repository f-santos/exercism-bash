#!/usr/bin/env bash

# Output to recall correct usage:
usage="Usage: error_handling.sh <person>"

# Must have exactly one argument
if [[ $# -ne 1 ]]
then
    echo "$usage"
    exit 1
fi

# Display correct output:
echo "Hello, $1"
