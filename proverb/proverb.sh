#!/usr/bin/env bash

# Display a sequence of sentences:
for (( i = 1, j = 2; i < $# ; i++, j++ ))
do
    echo "For want of a ${!i} the ${!j} was lost."
done

# Display final sentence:
if (( $# >= 1 ))
then
   echo "And all for the want of a $1."
fi
