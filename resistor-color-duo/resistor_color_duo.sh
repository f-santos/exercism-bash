#!/usr/bin/env bash

# Create an associative array
# (cf. p. 97 of Bash manual):
declare -A colors
colors=( ["black"]=0
         ["brown"]=1
         ["red"]=2
         ["orange"]=3
         ["yellow"]=4
         ["green"]=5
         ["blue"]=6
         ["violet"]=7
         ["grey"]=8
         ["white"]=9 )

# Display the values associated to
# the first two keys:
result=""

for color in "${@:1:2}"
do
    if [[ -v "colors[$color]" ]]
    then
        result+=${colors[$color]}
    else
        echo "invalid color"
        exit 1
    fi
done

# Display final result:
echo $result
exit 0
