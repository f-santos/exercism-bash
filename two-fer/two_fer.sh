#!/usr/bin/env bash

# Give a default value to the first argument:
yourname=${1:-you}

# Display final string:
echo "One for $yourname, one for me."
