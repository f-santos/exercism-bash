#!/usr/bin/env bash

# Check validity of arguments:
if [[ $# -ne 1 || ! $1 =~ ^[0-9]*$ ]]
then
    echo "Usage: leap.sh <year>"
    exit 1
fi

# Check whether we have a leap year:
if (($1 % 400 == 0)) || ( (($1 % 4 == 0)) && (($1 % 100 != 0)) )
then
    echo "true"
else
    echo "false"
fi

# Return 0 regardless of the result (true/false),
# just to say that the script ran with no error:
exit 0
