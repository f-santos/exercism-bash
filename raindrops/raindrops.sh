#!/usr/bin/env bash

rainsounds()
{
    (( $1 % 3 == 0 )) && echo -n "Pling"
    (( $1 % 5 == 0 )) && echo -n "Plang"
    (( $1 % 7 == 0 )) && echo -n "Plong"
}

result=$(rainsounds "$1")

echo ${result:-"$1"}
