#!/usr/bin/env bash

# Comments:
# I understand that a function is not necessary here,
# but I just wanted to get used to write functions in Bash :-)

hello () {
    echo "Hello, World!"
}

hello
