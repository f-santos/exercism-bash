#!/usr/bin/env bash

# Comment:
# I do know that there exists a function "rev",
# and the exercise could be solved with sth like:
# echo "$1" | rev
# But I want to try a manual solution with a for loop.

# Local variables:
len=$((${#1} - 1))   # string length (starting at 0)
reversed=""	     # will contain reversed string

# Reverse string:
for (( i = $len; i >= 0; i-- )); do
    reversed+=${1:i:1}
done

# Display result:
echo "$reversed"
